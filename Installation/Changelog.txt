From 2020-01-10 updates and changes are described on this page

2020-01-11 filePick.py is replaced by filePicklist.py due to security issues with folder traversal
           login.py is changed for filePicklist entrances. The issues are solved with the new version.
           Forms: from several filenames spaces removed, for Linux can't handle spaces in filenames.
2020-01-12 filePicklist.py more listitems in combobox then 10 in Linux, adjustment with setStyleSheet.
           filePicklist.py font, color  and sort items adjustments for QComboBox applied.
           filepicklist.py number of copies supplied
2020-01-13 filePicklist.py warning added if no filename is selected.
           toonGrafieken.py colors differentiated graph lines
           LINUX Installation.txt changed for install python3 and pip3 installation
           for python3 is installed on newest version Unbuntu
           bestelOrder.py added close button for changing quantity
2020-01-14 For Linux added font Sans Serif in LINUX Install.txt
2020-01-15 proefrun.py path adjusted for application in Linux.
           filePicklist.py adjusted for showing only text files
           LINUX Install.txt installation procedure tested and changed.
2020-01-18 bestelOrder.py 325 removed mhoev.text() (mhoev is already float)
2020-01-19 bestelOrder.py 328 333 error handling return articles changed
2020-01-20 bestelOrder.py cosmetic changes
2020-01-20 magvrdGrafiek.py cosmetic change
2020-01-22 webRetouren.py for clarity reasons setReadonly changed in setDisabled
           invoerDienstenorder.py notification of successful data entry added
           wijzDienstenorder.py procedure change order lines/next order lines improved
2020-01-23 invoerDienstenorder.py costs added to works adjusted for costs already partially booked with cluster Calculation
           wijzDienstenorder.py costs added to works adjusted for costs already partially booked with cluster Calculation
           wijzInkooporder.py procedure change order lines/next order lines improved
           wijzArtikel.py QCombobox items eenheid and categorie changed to startup with current item +
           items changed as items in database
           invoerArtikelen.py QCombobox categorie items changed as items in database
           urenMutatie.py restriction removed for exhanging employees from internal works
           urenimutatie.py restricton removed for exchanging employees from external works
2020-01-24 invoerArtikelen.py and wijzInkooporders.py cosmetic changes
2020-01-24 All pictures in folder Pandora/images/logos/Betaal repaired with imagestack mogrify to avoid warning
2020-01-25 login.py adjusted due too menu lines behaviour in Linux over 2 lines
2020-01-25 wijzArtikel.py vertical positioning window changed
2020-01-25 Pandora.py in main section added style Windows for Windows or style Fusion for Linux.
2020-01-27 bestelOrder.py payform headerbuttons iDEAL and Creditcards replaced by images.
2020-01-27 opvrLoonbetalingen.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrWerknemers.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrDienstenorders.py QTableview period , changed in period .
2020-01-27 opvrWerken.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrInternorders.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrClusters.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrIclusters.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrCluster_artikelen.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrIcluster_artikelen.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrClustercalculatie.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrIclustercalculatie.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrArtikelmutaties.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrDienstenmutaties.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrMateriaallijsten.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 afdrBetalen.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrArtikelen.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrInkooporders.py all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrReserveringen all floats round up with maximum  2 digits floating point+font black for clarity.
2020-01-27 opvrWerknperiode.py font black for clarity
2020-01-28 Several programs font, color adjustments for QComboBox applied.
2020-01-28 opvrLoonbetalingen.py changed window position search window
2020-01-28 opvrWerknemers.py floats on selection window right Aligned.
2020-01-28 opvrWerknperiode.py floats on selection window right Aligned.
2020-01-28 opvrWerken.py floats on selection window right Aligned.
2020-01-28 opvrWebverkorders.py floats on selection window right Aligned.
2020-01-28 login.py added entrance 5 for opvrReserveringen in Menu Voorraadbeheersing.
2020-01-28 opvrReserveringen.py floats on selection window right Aligned.
2020-01-28 opvrParams.py floats on selection window right Aligned.
2020-01-28 opvrKlantenorders.py floats on selection window right Aligned.
2020-01-28 opvrInternorders.py floats on selection window right Aligned.
2020-01-28 opvrInkooporders.py floats on selection window right Aligned.
2020-01-28 opvrIclusters.py floats on selection window right Aligned.
2020-01-28 opvrIclustercalculatie.py floats on selection window right Aligned.
2020-01-28 opvrIcluster_artikelen.py floats on selection window right Aligned.
2020-01-28 opvrDienstenorders.py floats on selection window right Aligned.
2020-01-28 opvrClusters.py floats on selection window right Aligned.
2020-01-28 opvrClustercalculatie.py floats on selection window right Aligned.
2020-01-28 opvrCluster_artikelen.py floats on selection window right Aligned.
2020-01-28 opvrCluster_artikelen.py floats on selection window right Aligned.
2020-01-28 opvrLoonbetalingen.py floats on selection window right Aligned.
2020-01-28 wijzArtikel.py search item adjusted on item reservations
2020-01-28 bestelOrder.py floats round up with maximum 2 digits+floats on selection window right Aligned.
2020-01-28 opvrDienstenorders.py fonts adjusted text to black for clarity.
2020-01-29 afdrBetalen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 artikelAfroep.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 artikelbestellingen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 bestelOrder.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 dienstenMutaties.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned 
2020-01-29 invoerClustercalculatie.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned 
2020-01-29 invoerCluster_artikelen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 invoerIclustercalculatie.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned 
2020-01-29 invoerIcluster_artikelen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned 
2020-01-29 magUitgifte.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrArtikelen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned 
2020-01-29 opvrArtikelmutaties.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrClustercalculatie.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrClusters.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrCluster_artikelen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrDienstenmutaties.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrDienstenorders.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrIclustercalculatie.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrIclusters.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrIcluster_artikelen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrInkooporders.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrInternorders.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrKlantenorders.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrLoonbetalingen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrMateriaallijsten.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrParams.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrReserveringen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrUrenmutaties.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrWebverkorders.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrWerken.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrWerknemers.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 printFacturen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 proefrun.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 toonResultaten.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 uitbetalenLonen.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 webRetouren.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzArtikel.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzClusters.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzDienstenorder.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzIclusters.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzigParams.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzInkooporder.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzInternorder.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzLeverancier.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzLoontabel.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzVerkoopbedrijf.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzWerken.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzWerknemer.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 wijzWerktarief.py in QTableView floats and integers right aligned + in showselection fonts changed and floats aligned
2020-01-29 opvrAccounts.py loonID + werknemer in QTableView added.
2020-01-30 login.py Combobox Menu items width adjusted from 280 to 290 due too scaling in Linux
2020-01-30 opvrReserveringen.py tableview set columns movevable with mouse so you can move colums bestelsaldo 
           and reserveringsaldo in the view area.
2020-01-30 dienstenMutaties.py button Muteren changed in Muteren / Sluiten (close).
2020-01-30 opvrUrenmutaties.py added menu choice searching on (part) last name.
2020-01-31 login.py possibility added to logon with emailadres or clientnumber (accountID)

2020-02-01 several script QTableView align floats
2020-02-01 at last found a good solution for alignment floats and integers 45 scripts adjusted with textroles.
2020-02-01 Login.png screenshot changed due to dual logon possibilities.
2020-02-01 opvrCluster_artikelen added notification if no item lines are present.
2020-02-01 opvrIcluster_artikelen added notification if no item lines are present.
2020-02-01 retourPortalWeb.py added dual indentification with email or Klantnummer (accountID).
2020-02-02 opvrLeverancier.py and opvrVerkoopbedrijven.py aligned integers from QTableView
2020-02-02 bestelOrder.py text changed notification stock shortage.
2020-02-03 urenMutaties.py for reasons of fast data entry notification screens removed and replaced
           by Muteren button gets colored green if entry succeeded and red if entry failed.
2020-02-03 urenMutaties.py for reasons of fast data entry notification screens removed and replaced
           by Muteren button gets colored green if entry succeeded and red if entry failed.
2020-02-03 login.py changed for preceding entries.
2020-02-04 urenMutaties.py applied info button and entry Datum Werkzaamheden made 'sticky' as Accountnummer and Werknummer.
           Info button and infoscreen added.
2020-02-04 urenImutaties.py applied info button and entry Datum Werkzaamheden made 'sticky' as Accountnummer and Werkorder.
           Info button and infoscreen added
2020-02-04 login.py entries changed according to returnvalue Datum Werkzaamheden.
2020-02-04 several scripts applied alignment for integers.
2020-02-05 11 scripts  adjusted alignments
2020-02-05 uitbetalenLonen.py indentification error fixed.
2020-02-05 12 scripts  adjusted alignments (alignment finished!)
2020-02-06 Added Documentation. First concept added for Stock. The Documnentation is made with Libre-Office
2020-02-06 wijz.Clusters.py calling notification noRecord forgotten (). Added now.
2020-02-06 wijzArtikelen.py changed categorie
2020-02-06 invoerArtikelen.py changed categorie
2020-02-07 documentation is converted to pdf (readability with github)
2020-02-07 documentation statement added
2020-02-07 documentation added table of contents
2020-02-07 documentation added accounts and access control.
2020-02-08 login.py accounting access to 7. requesting adding working hours employees.
2020-02-09 documentation Accounts_and_access_control expanded.
2020-02-10 documentation Calculation_material_workrates_and_services added.
2020-02-10 documentation Statement supplemented with some reflections
2020-02-11 opvrClustercalculatie.py fixed searching on worknumber.
2020-02-11 dienstenMutaties.py changed text in thirst seacrch item from 'Alle mutaties' to 'Alle diensten'
2020-02-11 documentation Works_and_financial_accountability added.
2020-02-11 voortgangGrafiek.py color background search submenu changed.
2020-02-11 toonGrafieken.py color background search submenu changed.
2020-02-11 opvrArtikelen.py ordering categories changed  by reservations and minimum stock for ordering.